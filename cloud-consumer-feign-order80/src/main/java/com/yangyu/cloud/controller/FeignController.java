package com.yangyu.cloud.controller;

import com.yangyu.cloud.service.PaymentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 一碗鱼汤面
 * @version V1.0
 * @Package com.yangyu.cloud.controller
 * @date 2021/2/21 21:36
 */
@RestController
public class FeignController {

    @Resource
    private PaymentService paymentService;

    @GetMapping(value = "/consumer/lb")
    public String getPaymentLB() {
        return paymentService.getPaymentLB();
    }
}
