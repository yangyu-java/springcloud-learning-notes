package com.yangyu.cloud.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author 一碗鱼汤面
 * @version V1.0
 * @Package com.yangyu.cloud.service
 * @date 2021/2/21 21:32
 */
@Component
@FeignClient(value = "CLOUD-PAYMENT-SERVICE")
public interface PaymentService {

    @GetMapping(value = "/payment/lb")
    String getPaymentLB();
}
