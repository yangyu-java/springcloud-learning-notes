package com.yangyu.springcloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author 一碗鱼汤面
 * @version V1.0
 * @Package com.yangyu.springcloud.controller
 * @date 2021/2/21 15:49
 */
@RestController
public class ZkController {
    public static final String URL = "http://cloud-payment-service";

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/consumer/zk")
    public String zkLb() {
        String s = restTemplate.getForObject(URL + "/zk", String.class);
        return s;
    }
}
