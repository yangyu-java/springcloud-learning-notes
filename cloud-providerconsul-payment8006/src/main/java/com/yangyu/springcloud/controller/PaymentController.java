package com.yangyu.springcloud.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author 一碗鱼汤面
 * @version V1.0
 * @Package com.yangyu.springcloud.controller
 * @date 2021/2/21 19:29
 */
@RestController
@Slf4j
public class PaymentController {
    @Value("${server.port}")
    private String serverPort;

    @RequestMapping(value = "/payment/consul")
    public String paymentConsul() {
        return "spring cloud with consul: " + serverPort + "\t   "+ UUID.randomUUID().toString();
    }
}
