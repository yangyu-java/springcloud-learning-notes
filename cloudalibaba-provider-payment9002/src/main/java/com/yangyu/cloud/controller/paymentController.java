package com.yangyu.cloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 一碗鱼汤面
 * @version V1.0
 * @Package com.yangyu.cloud.controller
 * @date 2021/2/25 21:16
 */
@RestController
public class paymentController {

    @Value("${server.port}")
    private int port;

    @GetMapping("/getPayment/nacos/{id}")
    public String getPayment(@PathVariable int id) {
        return "nacos payment port : " + port + " | id = " + id;
    }
}
