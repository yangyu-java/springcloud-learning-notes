package com.yangyu.myrule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 一碗鱼汤面
 * @version V1.0
 * @Package com.yangyu.myrule
 * @date 2021/2/21 20:33
 */
@Configuration
public class MyRule {
    @Bean
    public IRule mySelfRule() {
        return new RandomRule();
    }
}
