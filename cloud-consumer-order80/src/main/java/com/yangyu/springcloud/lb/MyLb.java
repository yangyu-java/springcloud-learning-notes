package com.yangyu.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;

/**
 * @author 一碗鱼汤面
 * @version V1.0
 * @Package com.yangyu.springcloud.lb
 * @date 2021/2/21 20:50
 */
public interface MyLb {
    ServiceInstance instance(List<ServiceInstance> serviceInstances);
}
