package com.yangyu.cloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author 一碗鱼汤面
 * @version V1.0
 * @Package com.yangyu.cloud.controller
 * @date 2021/2/25 21:39
 */
@RestController
public class OrderController {

    @Resource
    private RestTemplate restTemplate;

    @Value("${service-url.nacos-user-service}")
    private String url;

    @GetMapping("/consumer/getPayment/{id}")
    public String get(@PathVariable int id) {
        return restTemplate.getForObject(url + "/getPayment/nacos/" + id, String.class);
    }



}
